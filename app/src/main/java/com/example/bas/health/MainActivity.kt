package com.example.bas.health

import android.app.Fragment
import android.app.FragmentTransaction
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.example.bas.health.ui.InfoFragment
import com.example.bas.health.ui.ReportFragment
import com.example.bas.health.ui.TestFragment
import com.example.bas.health.ui.UserFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_test -> replaceFragment(TestFragment.instance)
            R.id.navigation_report -> replaceFragment(ReportFragment.instance)
            R.id.navigation_info -> replaceFragment(InfoFragment.instance)
            R.id.navigation_user -> replaceFragment(UserFragment.instance)
        }
        true
    }

    private fun replaceFragment(fragment: Fragment) {
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .replace(R.id.fragmentContainer, fragment)
                .commitAllowingStateLoss()
    }
}
