package com.example.bas.health.ui

import android.app.Fragment
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bas.health.R
import android.hardware.SensorEventListener
import kotlinx.android.synthetic.main.fragment_info.*


/**
 * Date: 26.04.2018
 * Time: 0:20
 *
 * @author bas
 */

class InfoFragment: Fragment(), SensorEventListener {

    companion object {
        val instance = InfoFragment()
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        textView?.text = p1.toString()
    }

    override fun onSensorChanged(p0: SensorEvent?) {

    }
}
